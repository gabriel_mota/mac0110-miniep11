using Test
using Unicode

function teste()
    @test palindromo("")
    @test palindromo("arara")
    @test palindromo("arará")
    @test palindromo("A mãe te ama.")
    @test palindromo("ovo")
    @test !palindromo("me passa kojo")
    @test palindromo("?ovo!")
    @test palindromo("?OVo!;")
    println("Fim dos testes :)")
end

function palindromo(str)
    str = lowercase(str)
    str = Unicode.normalize(str, stripmark=true)
    str = replace(str, r"[,;.!?: ]" => "")
    firstInd = firstindex(str)
    lastInd = lastindex(str)
    while firstInd <= lastInd
        if str[firstInd] != str[lastInd]
            return false
        end
        firstInd = nextind(str, firstInd)
        lastInd = prevind(str, lastInd)
    end
    return true
end

#teste()
